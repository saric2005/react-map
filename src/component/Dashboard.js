import React, {Component} from 'react';
import * as apis from '../apiClient';
import {Table, TableBody, TableHead, TableRow, TextTableCell, TextTableHeaderCell} from 'evergreen-ui';


export class Dashboard extends Component {

    state = {
        weather: {}
    }

    componentWillMount() {
        this.init();
    }

    init = () => {
        apis.getWeather(this.props.zip).then(weather => {
            this.setState({
                weather
            })
        });
    }

    render() {
        const {weather} = this.state;
        debugger
        return <div className="dashbaordTable">
            <Table>
                <TableHead>
                    <TextTableHeaderCell>
                        Car Identifier
                    </TextTableHeaderCell>
                    <TextTableHeaderCell>
                        Engine
                    </TextTableHeaderCell>
                    <TextTableHeaderCell>
                        Location
                    </TextTableHeaderCell>
                </TableHead>
                <TableBody height={240}>
                    {weather.map(car => (
                        <TableRow key={car.id} isSelectable>
                            <TextTableCell>{car.id}</TextTableCell>
                            <TextTableCell>{car.engine}</TextTableCell>
                            <TextTableCell>
                                {JSON.stringify(car.location, null, 4)}
                            </TextTableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </div>
    }


}
