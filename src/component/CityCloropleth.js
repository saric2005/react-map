import React, {Component} from 'react';

import {ComposableMap, Geographies, Geography, ZoomableGroup,} from "react-simple-maps";
import {scaleLinear} from "d3-scale";

const wrapperStyles = {
    width: "100%",
};

export class CityCloropleth extends Component {
    render() {

        const popScale = scaleLinear()
            .domain([0, 99999])
            .range(["#000000", "#FFFFFF"]);

        return (
            <div style={wrapperStyles}>
                <ComposableMap>
                    <ZoomableGroup zoom={1}>
                        <Geographies geography={"/zips_us_topo.json"}>
                            {(geographies, projection) => geographies.map((geography, i) => (
                                <Geography
                                    key={i}
                                    geography={geography}
                                    projection={projection}
                                    onClick={() => {
                                        this.props.history.push(`/dashboard/${geography.properties.zip}`)
                                    }}
                                    style={{
                                        default: {
                                            fill: popScale(geography.properties.zip),
                                            stroke: "#607D8B",
                                            strokeWidth: 0.75,
                                            outline: "none",
                                        }
                                    }}
                                />
                            ))}
                        </Geographies>
                    </ZoomableGroup>
                </ComposableMap>
            </div>
        )
    }
}