import React, {Component} from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import './App.css';
import {CityCloropleth} from "./component/CityCloropleth";
import {Dashboard} from "./component/Dashboard";

class App extends Component {
    render() {
        return (
            <Router>
                <div>
                    <Route exact path="/" component={CityCloropleth}/>
                    <Route path="/dashboard/:zip" render={({match}) => (
                        <Dashboard
                            zip={match.params.zip}
                        />
                    )}/>
                </div>
            </Router>
        );
    }
}

export default App;
